package it.unibo.test

trait ISomething {
  def f: Int
  def g: Int
}

object Something {
  def f = 10
  def g = 77
}