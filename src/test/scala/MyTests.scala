import org.scalatest.FunSuite
import it.unibo.test.{ISomething, Something}
import org.junit.runner.RunWith
import org.scalamock.scalatest.MockFactory
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class MyTests extends FunSuite with MockFactory {
  test("An empty Set should have size 0") {
    println("Test1")
    assert(Set.empty.isEmpty)
  }
  test("Some useless test") {
    println("Test2")
    val someStub = stub[ISomething]
    (someStub.f _).when().returns(42)
    assert(someStub.f == 42)
  }
}
