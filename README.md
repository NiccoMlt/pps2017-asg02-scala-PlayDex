# PlayDex #

Realization of a simple Pokédex implementing a REST client for PokeAPI with Scala language and Play WS library.

This project is developed with the aim of learning Scala language and OOFP (Object Oriented Funcional Programming).

## Links & Badges ##

### Trello board ###

https://trello.com/b/VSqPxJjC

### Travis CI ###

<center>

|                                                                        Stable branch                                                                        |                                                                      Development  branch                                                     |
|-------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------|
| [![Build Status](https://travis-ci.com/NiccoMlt/pps2017-asg02-scala-PlayDex.svg?branch=master)](https://travis-ci.com/NiccoMlt/pps2017-asg02-scala-PlayDex) | [![Build Status](https://travis-ci.com/NiccoMlt/pps2017-asg02-scala-PlayDex.svg?branch=develop)](https://travis-ci.com/NiccoMlt/pps2017-asg02-scala-PlayDex) |

</center>

### Codacy ###
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/7acf87c14fbe43fdbf638a4dfebebe55)](https://www.codacy.com/app/NiccoMlt/pps2017-asg02-scala-PlayDex?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=NiccoMlt/pps2017-asg02-scala-PlayDex&amp;utm_campaign=Badge_Grade)

## Assignment requirements: ##

> In this assignment, you are asked to put into practice what you have learnt about Scala and FP. 
> You will freely decide what to implement. 
> It can be based on some lab or something entirely different. 
> Just try to be excellent, functional, and exercise non-trivial things. 
> Detailed instructions are provided in the assignment template, plus the following:
> 
> 1. the latex template of the Report you have to produce is attached, with detailed instructions on what to do and how to document it;
> 2. the Downloads section of your repository will need to include your PDF version of the Report (we strongly encourage you to write in English and use the provided Latex template, but will tolerate if you write in Italian and use a different word processor);
> 3. you should properly use git to track your changes: recall to commit small and meaningful changes (we may use commits also to check if you actually worked more or less for the right amount of time) -- recall not to track binaries; 
> 4. deliver your assignment using this e-learning tool, by just providing the PDF report;
> 5. it is understood that you should not, for obvious reasons, modify the repo after you submitted;
> 6. if produced before the exam, this assignment will be used at the exam to check your knowledge on Scala: doing it properly will make the exam easier, doing it not so properly does not change things: you will be able to remedy at the exam so do not worry! 
> 
> Suggestion: exercise your skills now!

